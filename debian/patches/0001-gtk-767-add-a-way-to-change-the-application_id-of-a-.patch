From 2b74662b2befebb988b63118c8d7ee8c9d90e736 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Caol=C3=A1n=20McNamara?= <caolanm@redhat.com>
Date: Tue, 7 Jul 2020 21:12:59 +0100
Subject: [PATCH] gtk#767 add a way to change the application_id of a toplevel
 wayland GdkSurface

so LibreOffice can reuse toplevels and get the right task icons

references;
 https://gitlab.gnome.org/GNOME/gtk/-/issues/767
 https://lists.freedesktop.org/archives/wayland-devel/2019-July/040704.html
 https://gitlab.freedesktop.org/wayland/wayland-protocols/commit/e0d6ad1d5e7e80321285a9b14ca3329289eb02e6
 https://bugs.documentfoundation.org/show_bug.cgi?id=125934
 https://bugzilla.redhat.com/show_bug.cgi?id=1334915
 https://bugreports.qt.io/browse/QTBUG-77182

Origin: upstream, https://gitlab.gnome.org/GNOME/gtk/commit/2b74662b2befe
---
 gdk/wayland/gdkwaylandwindow.h  |  4 ++++
 gdk/wayland/gdkwindow-wayland.c | 42 ++++++++++++++++++++++++---------
 2 files changed, 35 insertions(+), 11 deletions(-)

diff --git a/gdk/wayland/gdkwaylandwindow.h b/gdk/wayland/gdkwaylandwindow.h
index 59fbd5727d..3b76822962 100644
--- a/gdk/wayland/gdkwaylandwindow.h
+++ b/gdk/wayland/gdkwaylandwindow.h
@@ -77,6 +77,10 @@ GDK_AVAILABLE_IN_3_22
 gboolean                 gdk_wayland_window_set_transient_for_exported (GdkWindow *window,
                                                                         char      *parent_handle_str);
 
+GDK_AVAILABLE_IN_3_24
+void                     gdk_wayland_window_set_application_id (GdkWindow *window,
+                                                                const char *application_id);
+
 GDK_AVAILABLE_IN_3_22
 void gdk_wayland_window_announce_csd                        (GdkWindow *window);
 
diff --git a/gdk/wayland/gdkwindow-wayland.c b/gdk/wayland/gdkwindow-wayland.c
index cc5e98e5f3..01b6a4c1b7 100644
--- a/gdk/wayland/gdkwindow-wayland.c
+++ b/gdk/wayland/gdkwindow-wayland.c
@@ -1933,6 +1933,36 @@ create_zxdg_toplevel_v6_resources (GdkWindow *window)
                                  window);
 }
 
+void
+gdk_wayland_window_set_application_id (GdkWindow *window, const char* application_id)
+{
+  GdkWindowImplWayland *impl;
+  GdkWaylandDisplay *display_wayland;
+
+  g_return_if_fail (application_id != NULL);
+
+  if (GDK_WINDOW_DESTROYED (window))
+    return;
+
+  if (!is_realized_toplevel (window))
+    return;
+
+  display_wayland = GDK_WAYLAND_DISPLAY (gdk_window_get_display (window));
+  impl = GDK_WINDOW_IMPL_WAYLAND (window->impl);
+
+  switch (display_wayland->shell_variant)
+    {
+    case GDK_WAYLAND_SHELL_VARIANT_XDG_SHELL:
+      xdg_toplevel_set_app_id (impl->display_server.xdg_toplevel,
+                               application_id);
+      break;
+    case GDK_WAYLAND_SHELL_VARIANT_ZXDG_SHELL_V6:
+      zxdg_toplevel_v6_set_app_id (impl->display_server.zxdg_toplevel_v6,
+                                   application_id);
+      break;
+    }
+}
+
 static void
 gdk_wayland_window_create_xdg_toplevel (GdkWindow *window)
 {
@@ -1988,17 +2018,7 @@ G_GNUC_END_IGNORE_DEPRECATIONS
   if (app_id == NULL)
     app_id = gdk_get_program_class ();
 
-  switch (display_wayland->shell_variant)
-    {
-    case GDK_WAYLAND_SHELL_VARIANT_XDG_SHELL:
-      xdg_toplevel_set_app_id (impl->display_server.xdg_toplevel,
-                               app_id);
-      break;
-    case GDK_WAYLAND_SHELL_VARIANT_ZXDG_SHELL_V6:
-      zxdg_toplevel_v6_set_app_id (impl->display_server.zxdg_toplevel_v6,
-                                   app_id);
-      break;
-    }
+  gdk_wayland_window_set_application_id (window, app_id);
 
   maybe_set_gtk_surface_dbus_properties (window);
   maybe_set_gtk_surface_modal (window);
-- 
2.20.1

